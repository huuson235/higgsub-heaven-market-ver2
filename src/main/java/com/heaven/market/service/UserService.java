package com.heaven.market.service;

import com.heaven.market.MD5.MD5;
import com.heaven.market.controller.dto.LoginDTO;
import com.heaven.market.controller.dto.UserDTO;
import com.heaven.market.enums.Role;
import com.heaven.market.model.User;
import com.heaven.market.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * Created by Administrator on 27/04/2016.
 */
@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;
    /*
    REGISTER
     */
    public User createUser(UserDTO userDTO){
       // userRepository.save(createAdmin());
        User user = new User();
        user.setUsername(userDTO.getUsername());
        user.setPassword(MD5.encryptMD5(userDTO.getPassword()));
        user.setEmail(userDTO.getEmail());
        user.setRole(userDTO.getRole());
        user.setToken(createToken());
        user.setExpireDate(new Date(System.currentTimeMillis() + 1000*60*60*24));
        user.setTrustPoint(0);
        return userRepository.save(user);
    }

    public List<UserDTO> getAllUsers(){
        List<User> users = (List<User>) userRepository.findAll();
        List<UserDTO> userDTOList = new ArrayList<UserDTO>();
        UserDTO userDTO = new UserDTO();
        for(User user:users ){
            userDTO.setUsername(user.getUsername());
            userDTO.setPassword(user.getPassword());
            userDTO.setRePassword(user.getPassword());
            userDTO.setRole(user.getRole());
            userDTO.setEmail(user.getEmail());
            userDTO.setTrustPoint(user.getTrustPoint());
            userDTOList.add(userDTO);
        }
        return userDTOList;
    }

    public User createAdmin(){
        User user = new User();
        user.setExpireDate(new Date(System.currentTimeMillis() + 1000*60*60*24));
        user.setToken(createToken());
        user.setPassword("admin");
        user.setEmail("admin@gmail.com");
        user.setTrustPoint(100);
        user.setRole(Role.ADMIN);
        user.setUsername("Admin");
        return user;
    }
    /*
    LOGIN
     */

    public String doLogin(LoginDTO loginDTO) {
        // 1. Generate token if not exist
        // 2. Set expired time for token
        User user = userRepository.findByUsername(loginDTO.getUsername());
        // username is not correct
        if (user == null) return "Username is not existed";
        // password is not correct
        if (!user.getPassword().equals(MD5.encryptMD5(loginDTO.getPassword())))
            return "Password is not correct";
        if (user.getToken() == null) {
            user.setToken(UUID.randomUUID().toString());
            user.setExpireDate(new Date(System.currentTimeMillis() + (1000 * 60 * 60 * 24)));
        } else {
            user.setExpireDate(new Date(System.currentTimeMillis() + (1000 * 60 * 60 * 24)));
        }
        user = userRepository.save(user);
        return user.getToken();
    }


    //FUNCTION EXTRA

    //Random UUID
    private String createToken(){
        return UUID.randomUUID().toString();
    }

    //Send and confirm by email
    private void sendEmail(){}

    public String checkUserDTO(UserDTO userDTO) {
        User user = userRepository.findByUsername(userDTO.getUsername());
        if (user != null) {
            return "Username has already been used to create an account.";
        }
        else return "success";
    }
}
