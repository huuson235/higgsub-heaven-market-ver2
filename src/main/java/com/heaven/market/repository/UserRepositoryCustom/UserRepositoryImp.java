package com.heaven.market.repository.UserRepositoryCustom;

import com.heaven.market.model.User;
import org.springframework.data.jpa.repository.support.QueryDslRepositorySupport;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Created by son on 29/04/2016.
 */
public class UserRepositoryImp implements UserRepositoryCustom {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public User findByUsername(String username) {
        return (User) this.entityManager.createQuery(
                "select u from Table USER u where u.username like '"+ username +"'").getSingleResult();
    }

    @Override
    public User findByToken(String token) {
        return (User) this.entityManager.createQuery(
                "select u from Table USER u where u.token like '"+ token +"'").getSingleResult();
    }
}
