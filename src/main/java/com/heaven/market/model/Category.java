package com.heaven.market.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.util.List;

/**
 * Created by Ultimates Spirit on 04/27/16.
 */
@Entity
@Table(name = "CATEGORY")
public class Category { // vs product
    @Id
    private String nameCategory;

    @ManyToMany (mappedBy = "categoryList")
    private List<Product> productList;

    public String getNameCategory() {
        return nameCategory;
    }

    public void setNameCategory(String nameCategory) {
        this.nameCategory = nameCategory;
    }

    public List<Product> getProductList() {
        return productList;
    }

    public void setProductList(List<Product> productList) {
        this.productList = productList;
    }
    //2 list doi dien -> 2 onetomany
    //tao bang trung gian-> manytomany11
}
