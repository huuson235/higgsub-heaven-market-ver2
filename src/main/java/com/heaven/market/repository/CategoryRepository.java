package com.heaven.market.repository;

import com.heaven.market.model.Category;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Ultimates Spirit on 04/27/16.
 */
@Repository
public interface CategoryRepository extends CrudRepository<Category, String>{
}
