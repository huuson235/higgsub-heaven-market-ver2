package com.heaven.market.service;

import com.heaven.market.SearchEngine.InvertedIndex;
import com.heaven.market.SearchEngine.InvertedIndexRepository;
import com.heaven.market.controller.dto.ProductDTO;
import com.heaven.market.enums.StatusProduct;
import com.heaven.market.model.Category;
import com.heaven.market.model.Comment;
import com.heaven.market.model.Product;
import com.heaven.market.model.User;
import com.heaven.market.repository.CategoryRepository;
import com.heaven.market.repository.CommentRepository;
import com.heaven.market.repository.ProductRepository;
import com.heaven.market.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by Administrator on 27/04/2016.
 */
@Service
public class ProductService {

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private InvertedIndexRepository invertedIndexRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private CommentRepository commentRepository;

    public List<ProductDTO> addProduct(String username, ProductDTO productDTO){
        User user = userRepository.findByUsername(username);
        if (user == null) return null;

        Product product = new Product();
        product.setNameProductVN(productDTO.getNameProductVN());
        product.setNameProductENG(unAccent(fixSpacesString(productDTO.getNameProductVN())));
        product.setContentIntro(productDTO.getContentIntro());
        product.setPriceProduct(productDTO.getPriceProduct());
        product.setStatus(productDTO.getStatusProduct());

        List<Category> categoryList = new ArrayList<Category>();
        for (String categoryName : productDTO.getCategoryList()) {
            Category categoryObj = categoryRepository.findOne(categoryName);
            if (categoryObj == null) {
                categoryObj = new Category();
                categoryObj.setNameCategory(categoryName);
                categoryObj.setProductList(new ArrayList<Product>());
            }
            categoryObj.getProductList().add(product);
            categoryList.add(categoryObj);
            categoryObj = categoryRepository.save(categoryObj);
        }

        product.setCategoryList(categoryList);
        product = productRepository.save(product);

        user.getProductList().add(product);
        user = userRepository.save(user);
        createInvertedIndex(product);

        return showProducts(username);

    }

    public List<ProductDTO> showProducts(String username){
        List<Product> productList = userRepository.findByUsername(username).getProductList();
        List<ProductDTO> productDTOList = new ArrayList<ProductDTO>();
        for(Product product : productList){
            ProductDTO productDTO = new ProductDTO(product);
            productDTOList.add(productDTO);
        }
        return productDTOList;
    }

    public boolean deleteProduct(String username, int productID) {
        User user = userRepository.findByUsername(username);
        if (user == null ) return false;

        Product deleteProduct = productRepository.findOne(productID);
        productRepository.delete(productID);
        deleteInvertedIndex(deleteProduct);
        return user.getProductList().remove(deleteProduct);
    }

    public List<Comment> addComment(int productID, Comment comment) {
        Product product = productRepository.findOne(productID);
        if (product == null) return null;
        comment = commentRepository.save(comment);
        product.getCommentList().add(comment);
        product = productRepository.save(product);
        return product.getCommentList();
    }
    /*
FUNCTION EXTRA
 */

    // remove mark of Vietnamese word.
    private String unAccent(String s) {
        String temp = Normalizer.normalize(s, Normalizer.Form.NFD);
        Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
        return  pattern.matcher(temp).replaceAll("").toLowerCase().replaceAll("đ", "d");
    }

    // Process space in string
    private static String fixSpacesString(String s) {
        return s.trim().replaceAll("\\s+", " ");
    }

    // Tao them du lieu cho InvertedIndex
    public void createInvertedIndex(Product product) {
        // Danh sach cac word trich xuat tu NameUnaccentProduct (ten sau khi bo dau)
        String[] wordList = product.getNameProductENG().split(" ");
        for (String word : wordList) {
            // key = word tuong ung trong inverted Index databse
            InvertedIndex key = invertedIndexRepository.findOne(word);
            if (key == null) { // key chua xuat hien trong database (new key)
                InvertedIndex newWord = new InvertedIndex(word, product.getProductID());
                invertedIndexRepository.save(newWord);
            } else {
                key.addSequence(product.getProductID());
            }
        }
    }
    // Xoa du lieu khi co 1 product bi xoa di
    public void deleteInvertedIndex(Product product) {
        String[] wordList = product.getNameProductENG().split(" ");
        for (String word : wordList) {
            InvertedIndex key = invertedIndexRepository.findOne(word);
            key.deleteProductID(product.getProductID());
        }
    }

    public List<Comment> deleteComment(int productID, int commentID) {
        Product product = productRepository.findOne(productID);
        Comment comment = commentRepository.findOne(commentID);
        product.getCommentList().remove(comment);
        product = productRepository.save(product);
        commentRepository.delete(commentID);

        return product.getCommentList();
    }
}


