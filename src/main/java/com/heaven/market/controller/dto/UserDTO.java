package com.heaven.market.controller.dto;

import com.heaven.market.enums.Role;

/**
 * Created by Administrator on 27/04/2016.
 */
public class UserDTO {
    private String username;
    private String password;
    private String rePassword;
    private String email;
    private String token;
    private float trustPoint;
    private Role role;
    private String newPassword;
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getRePassword() {
        return rePassword;
    }

    public void setRePassword(String rePassword) {
        this.rePassword = rePassword;
    }

    public float getTrustPoint() {
        return trustPoint;
    }

    public void setTrustPoint(float trustPoint) {
        this.trustPoint = trustPoint;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }
}
