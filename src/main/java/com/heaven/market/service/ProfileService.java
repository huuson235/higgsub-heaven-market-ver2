package com.heaven.market.service;

import com.heaven.market.MD5.MD5;
import com.heaven.market.controller.dto.UserDTO;
import com.heaven.market.model.User;
import com.heaven.market.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Administrator on 27/04/2016.
 */
@Service
public class ProfileService {
    @Autowired
    private UserRepository userRepository;

    public boolean changePassword(UserDTO userDTO) {
        User user = userRepository.findOne(userDTO.getToken());
        String newPassword = MD5.encryptMD5(userDTO.getNewPassword());

        if (user.getPassword().equals(userDTO.getPassword())){
            user.setPassword(newPassword);
        } else return false;
        user = userRepository.save(user);
        return true;
    }
}
