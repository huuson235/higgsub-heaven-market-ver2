package com.heaven.market.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by son on 28/04/2016.
 */
@Entity
@Table(name = "COMMENT")
public class Comment {
    @Id
    private int commentID;
    private String username; // username cua nguoi comment

    public int getCommentID() {
        return commentID;
    }

    public void setCommentID(int commentID) {
        this.commentID = commentID;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
