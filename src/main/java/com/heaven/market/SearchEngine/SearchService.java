package com.heaven.market.SearchEngine;

import com.heaven.market.controller.dto.ProductDTO;
import com.heaven.market.model.Category;
import com.heaven.market.model.Product;
import com.heaven.market.repository.CategoryRepository;
import com.heaven.market.repository.ProductRepository;
import com.heaven.market.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by son on 28/04/2016.
 */
@Service
public class SearchService {
    @Autowired
    private InvertedIndexRepository invertedIndexRepository;
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private ProductService productService;
    @Autowired SearchFunctions searchFunctions;
    @Autowired
    private CategoryRepository categoryRepository;

    public List<ProductDTO> searchByName(String query) {
        // Xoa table invertedIndex cu~ di, lam cai moi' (2 dong code nay chi phuc vu viec test)
        invertedIndexRepository.deleteAll();
        // Cai nay chi dung de update lai danh sach, khi su dung that su thi ko can
        searchFunctions.updateInverted();

        List<Product> foundProducts = new ArrayList<Product>();
        query = searchFunctions.fixQuery(query);
        String[] wordList = query.split(" ");

        // Danh sach cac word can kiem tra.
        List<InvertedIndex> keyList = new ArrayList<InvertedIndex>();
        for (String word : wordList) {
            InvertedIndex temp = invertedIndexRepository.findOne(word);
            if (temp != null) {
                keyList.add(temp);
            }
        }

        if (keyList.size() == 0) return null; // size() = 0 => ko tim thay tu khoa' nao hop le

        List<Integer> productIDlist = searchFunctions.intersection(keyList);
        for (int productID : productIDlist) {
            foundProducts.add(productRepository.findOne(productID));
        }
        List<ProductDTO> productDTOList = new ArrayList<ProductDTO>();
        for (Product product : foundProducts) {
            productDTOList.add(new ProductDTO(product));
        }
        return productDTOList;
    }

    public List<ProductDTO> searchByCategory(String categoryQuery) {
        Category category = categoryRepository.findOne(categoryQuery);
        if (category == null) return null;
        List<ProductDTO> productDTOList = new ArrayList<ProductDTO>();
        for (Product product : category.getProductList()) {
            productDTOList.add(new ProductDTO(product));
        }
        return productDTOList;
    }
}
