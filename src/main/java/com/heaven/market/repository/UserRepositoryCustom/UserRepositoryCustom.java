package com.heaven.market.repository.UserRepositoryCustom;

import com.heaven.market.model.User;

/**
 * Created by son on 29/04/2016.
 */
public interface UserRepositoryCustom {
    public User findByUsername(String username);
    public User findByToken(String token);
}
